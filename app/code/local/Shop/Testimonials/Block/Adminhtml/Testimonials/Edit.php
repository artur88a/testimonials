<?php

class Shop_Testimonials_Block_Adminhtml_Testimonials_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'testimonials';
        $this->_controller = 'adminhtml_testimonials';

        $this->_updateButton('save', 'label', Mage::helper('testimonials')->__('Save Testimonials'));
        $this->_updateButton('delete', 'label', Mage::helper('testimonials')->__('Delete Testimonials'));

        if( $this->getRequest()->getParam($this->_objectId) ) {
            $model = Mage::getModel('testimonials/testimonials')->load($this->getRequest()->getParam($this->_objectId));
            Mage::register('testimonials', $model);
        }
    }

    public function getHeaderText()
    {
        if( Mage::registry('testimonials') && Mage::registry('testimonials')->getId() ) {
            return Mage::helper('testimonials')->__('Edit Testimonials');
        } else {
            return Mage::helper('testimonials')->__('Add Testimonials');
        }
    }
}
