<?php

class Shop_Testimonials_Block_Adminhtml_Testimonials_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
        ));

        $fieldset = $form->addFieldset('testimonials_form', array(
            'legend'	  => Mage::helper('testimonials')->__('Testimonials'),
            'class'		=> 'fieldset-wide',
            )
        );

        $fieldset->addField('name', 'text', array(
            'name'      => 'name',
            'label'     => Mage::helper('testimonials')->__('Name'),
            'class'     => 'required-entry',
            'required'  => true,
        ));

        $fieldset->addField('email', 'text', array(
            'name'      => 'email',
            'label'     => Mage::helper('testimonials')->__('Email'),
            'class'     => 'required-entry',
            'required'  => true,
        ));

        $fieldset->addField('title', 'text', array(
            'name'      => 'title',
            'label'     => Mage::helper('testimonials')->__('Title'),
            'class'     => 'required-entry',
            'required'  => true,
        ));

        $fieldset->addField('content', 'editor', array(
            'name'      => 'content',
            'label'     => Mage::helper('testimonials')->__('Content'),
            'title'     => Mage::helper('testimonials')->__('Content'),
            'style'     => 'width:100%;height:300px;',
            'wysiwyg'   => true,
			'required'  => true,
        ));

        if (Mage::registry('testimonials')) {
            $form->setValues(Mage::registry('testimonials')->getData());
        }

        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }

}
