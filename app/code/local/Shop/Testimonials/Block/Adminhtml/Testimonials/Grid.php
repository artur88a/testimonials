<?php

class Shop_Testimonials_Block_Adminhtml_Testimonials_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('testimonialsGrid');
        $this->setDefaultDir('ASC');
    }

    protected function _prepareCollection()
    {
        $this->setCollection(Mage::getModel('testimonials/testimonials')->getCollection());
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
		$this->addColumn('id', array(
			'header' => Mage::helper('testimonials')->__('ID'),
			'align' => 'center',
			'width' => '30px',
			'index' => 'id',
		));

        $this->addColumn('customer_id', array(
            'header'    => Mage::helper('testimonials')->__('CustomerID'),
            'align'     => 'right',
            'width'     => '50px',
            'index'     => 'customer_id',
            'type'      => 'number',
        ));

        $this->addColumn('name', array(
            'header'    => Mage::helper('testimonials')->__('Name'),
            'align'     => 'left',
            'index'     => 'name',
        ));

        $this->addColumn('email', array(
            'header'    => Mage::helper('testimonials')->__('Email'),
            'align'     => 'left',
            'index'     => 'email',
        ));

		$this->addColumn('title', array(
            'header'    => Mage::helper('testimonials')->__('Title'),
            'align'     => 'left',
            'index'     => 'title',
        ));

        $this->addColumn('content', array(
            'header'    => Mage::helper('testimonials')->__('Content'),
            'align'     => 'left',
            'index'     => 'content',
        ));

		$this->addColumn('created', array(
			'header' => Mage::helper('testimonials')->__('Created'),
			'index' => 'created',
			'type' => 'datetime',
			'width' => '145px',
		));

		$this->addColumn('updated', array(
			'header' => Mage::helper('testimonials')->__('Updated'),
			'index' => 'updated',
			'type' => 'datetime',
			'width' => '145px',
		));

		$this->addColumn('action',
				array(
					'header' => Mage::helper('testimonials')->__('Action'),
					'width' => '80',
					'type' => 'action',
					'getter' => 'getId',
					'actions' => array(
						array(
							'caption' => Mage::helper('testimonials')->__('Edit'),
							'url' => array('base' => '*/*/edit'),
							'field' => 'id'
						)
					),
					'filter' => false,
					'sortable' => false,
					'index' => 'stores',
					'is_system' => true,
		));

        return parent::_prepareColumns();
    }

	protected function _prepareMassaction() {
		$this->setMassactionIdField('id');
		$this->getMassactionBlock()->setFormFieldName('testimonials');
		$this->getMassactionBlock()->addItem('delete', array(
			'label' => Mage::helper('testimonials')->__('Delete'),
			'url' => $this->getUrl('*/*/massDelete'),
			'confirm' => Mage::helper('testimonials')->__('Are you sure?')
		));
		return $this;
	}

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}
