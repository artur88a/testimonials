<?php

class Shop_Testimonials_Block_Testimonials extends Mage_Core_Block_Template
{
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $pager = $this->getLayout()->createBlock('page/html_pager', 'custom.pager');
        $pager->setAvailableLimit(array(5=>5,10=>10,20=>20,30=>30,'all'=>'all'));


        $collection = Mage::getModel('testimonials/testimonials')->getCollection()->setOrder('created', 'DESC');

        if(Mage::app()->getRequest()->getParam('p') > 0) {
            $collection->setCurPage(Mage::app()->getRequest()->getParam('p'));
        }
        if(Mage::app()->getRequest()->getParam('limit') > 0) {
            $collection->setPageSize($this->getRequest()->getParam('limit'));
        }
        $collection->load();

        $pager->setCollection($collection);
        $this->setChild('pager', $pager);

        $this->setTestimonials($collection);
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}
