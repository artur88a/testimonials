<?php

class Shop_Testimonials_Adminhtml_TestimonialsController extends Mage_Adminhtml_Controller_Action
{

	protected function _isAllowed()
	{
		return Mage::getSingleton('admin/session')->isAllowed('testimonials/testimonials');
	}

	protected function _initAction()
	{
		$this->loadLayout();
		$this->_setActiveMenu('testimonials/testimonials');
		$this->_addBreadcrumb(Mage::helper('testimonials')->__('Testimonials'), Mage::helper('testimonials')->__('Testimonials'));
	}


	public function indexAction()
	{
		$this->loadLayout();
		$this->_setActiveMenu('Testimonials');
		$contentBlock = $this->getLayout()->createBlock('testimonials/adminhtml_testimonials');
		$this->_addContent($contentBlock);
		$this->renderLayout();
	}


	public function editAction()
	{
		$this->_initAction();
		$this->_addContent($this->getLayout()->createBlock('testimonials/adminhtml_testimonials_edit'));
		$this->renderLayout();
	}

	public function newAction()
	{
		$this->editAction();
	}
	public function saveAction()
	{
		// Save Store
		if ($data = $this->getRequest()->getPost()) {
			$model = Mage::getModel('testimonials/testimonials');
			$id = $this->getRequest()->getParam('id');
			if($id){
				$model->load($id);
			}
			$userData = Mage::getSingleton('admin/session');

			try {
				if ($model->getData('customer_id') == null) {
					$model->setData('customer_id',$userData->getUser()->getUserId());
				}
				$model->setData('name',$data['name']);
				$model->setData('email',$data['email']);
				$model->setData('title',$data['title']);
				$model->setData('content',$data['content']);

				if ($model->getCreated() == null) {
					$model->setCreated(now());
				}
				else {
					$model->setUpdated(now());
				}
				$model->save();

				$id = $model->getId();

				Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Testimonials was saved successfully'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);
				$this->_redirect('*/*/');

			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				Mage::getSingleton('adminhtml/session')->setFormData($data);
				$this->_redirect('*/*/edit', array(
					'id' => $id
				));
			}
			return;
		}
		Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find item to save'));
		$this->_redirect('*/*/');
	}

	public function deleteAction()
	{
		if ($this->getRequest()->getParam('id') > 0) {
			try {
				$model = Mage::getModel('testimonials/testimonials');
				$model->setId($this->getRequest()->getParam('id'))
				      ->delete();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('testimonials')->__('Testimonial was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}

		$this->_redirect('*/*/');
	}

	public function massDeleteAction() {
		$bannerIds = $this->getRequest()->getParam('testimonials');
		if (!is_array($bannerIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
		} else {
			try {
				foreach ($bannerIds as $bannerId) {
					$model = Mage::getModel('testimonials/testimonials')->load($bannerId);
					$_helper = Mage::helper('testimonials');
					$model->delete();
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(
						Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($bannerIds)));
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}

}
