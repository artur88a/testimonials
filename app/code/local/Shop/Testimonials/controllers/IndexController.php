<?php
class Shop_Testimonials_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function viewAction()
    {
        $timId = Mage::app()->getRequest()->getParam('id', 0);
        $tim = Mage::getModel('testimonials/testimonials')->load($timId);

        if ($tim->getId() > 0) {
            $this->loadLayout();
            $this->getLayout()->getBlock('testimonial')->assign(array(
                "msgItem" => $tim,
            ));
            $this->renderLayout();
        } else {
            $this->_forward('noRoute');
        }
    }
}
?>
