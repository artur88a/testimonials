<?php

class Shop_Testimonials_SubmitController extends Mage_Core_Controller_Front_Action
{
	public function indexAction()
	{
		$this->loadLayout();
		$this->renderLayout();
	}

	public function saveAction() {
		$data = $this->getRequest()->getPost();
        if (!empty($data)) {
			if(Mage::getSingleton('customer/session')->isLoggedIn()) {
				$session = Mage::getSingleton('core/session', array('name' => 'frontend'));

				$testimonial = Mage::getModel('testimonials/testimonials');
				try {
					$customerData = Mage::getModel('customer/session')->getCustomer();

					$testimonial->setCustomerId($customerData['entity_id']);
					$testimonial->setName($customerData['firstname']);
					$testimonial->setEmail($customerData['email']);
					$testimonial->setTitle($data['title']);
					$testimonial->setContent($data['content']);
					if ($testimonial->getCreated() == null) {
						$testimonial->setCreated(now());
					} else {
						$testimonial->setUpdated(now());
					}
					$testimonial->save();

					$session->addSuccess($this->__('Your testimonial has been accepted'));
				} catch (Exception $e) {
					$session->setFormData($data);
					$session->addError($this->__('Unable to post testimonial. Please, try again later !'));
				}
			}
        }

        if ($redirectUrl = Mage::getSingleton('core/session')->getRedirectUrl(true)) {
            $this->_redirectUrl($redirectUrl);
            return;
        }
        $this->_redirectReferer();
	}
}
